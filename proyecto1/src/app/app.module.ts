import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContenidoMateria } from './contenido/contenido';
import { IntroMateria } from './intro/intro';
import { InfoMateria } from './info/info';
import { CompetenciaMateria } from './competencia/competencia';
import { ContactosMateria } from './contactos/contactos';


@NgModule({
  declarations: [
    AppComponent, ContenidoMateria , IntroMateria, InfoMateria, CompetenciaMateria, ContactosMateria
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent, ContenidoMateria, IntroMateria, InfoMateria, CompetenciaMateria, ContactosMateria]
})
export class AppModule { }
